#!/usr/bin/env rvm-shebang-ruby
#coding: utf-8

#requires
require('sequel')
require('log4r')
require('feed-normalizer')
require('uuid')
require('mechanize')
require('tmpdir')
require('singleton')
require('xmlrpc/client')
require('pp')
require('net/http')
require('tempfile')
require('feedjira')
require('filemagic')

SCRIPT_DIR = File.dirname(File.expand_path(__FILE__))

#lib paths
$LOAD_PATH.unshift(SCRIPT_DIR + '/../lib')

require('minds/pluggable')
require('minds/transmission-client')
require('minds/app')
require('minds/schema')
require('minds/config')

#init logger
logger = Log4r::Logger['logger']
if (__FILE__ == $0)
    logger = Log4r::Logger.new('logger')
    #logger.add( Log4r::Outputter.stderr )

    p = Log4r::PatternFormatter.new(:pattern => "[%d %x] %l - %m")
    outputters = [
        {
            :type => 'file',
            :params => {
                :filename => SCRIPT_DIR + '/app.log',
                :level    => Log4r::INFO
            }
        },

        {
            :type => 'file',
            :params => {
                :filename => SCRIPT_DIR + '/debug.log',
                :level    => Log4r::DEBUG
            }
        },
    ]

    outputters.each do |item|
        outputter = Log4r::FileOutputter.new(
            item[:type],
            item[:params]
        )
        outputter.formatter = p
        logger.add( outputter )
    end

    # log level order is DEBUG < INFO < WARN < ERROR < FATAL
    #logger.level = Log4r::DEBUG
end

require('exceptions')
require('process')
require('feed_type')
require('subtitle')
require('movie_hasher')
require('schema/default')

module Series
    class App < Minds::App
        include(Singleton)

        attr_reader(:root, :config, :schema)

        def initialize()
            #app root folder
            @root = SCRIPT_DIR

            #config
            @config = Minds::Config.create(
                :type => :YAML,
                :file => "#{ @root }/../etc/config.yaml"
            )

            #init schema to nil
            @schema = nil
        end

        class << self
            def method_missing(method, *args, &block)
                return self.instance.send(method, *args, &block)
            end
        end

        def logger()
            return Log4r::Logger['logger']
        end

        def start()
            #change current dir to root
            Dir.chdir(@root)

            @schema = Series::Schema::Default.new(
                :dsn => @config['dsn']
            )

            #get from config the active processes
            processes = self.config.get('processes')
            while (true)
                processes.each do |item|
                    name = item.keys[0]
                    conf = item[name]
                    #raise([name, conf].inspect)

                    cur_time = Time.new.to_i

                    #skip process run if next_run is bigger than curren time
                    if (conf['next_run'] && conf['next_run'] > cur_time)
                        next
                    end

                    process = conf['process']
                    if (!process)
                        process = Series::Process.plugin(name)
                        conf['process'] = process
                    end

                    #create a fork to execute the process
                    process_child = fork do
                        $PROGRAM_NAME = process.process_name

                        #create instance of process
                        process = process.new(self)

                        #execute process
                        process.run

                        exit(0)
                    end

                    #wait until the child doesn't finish
                    ::Process.waitpid(process_child)

                    #set next run
                    conf['next_run'] = (Time.new + (conf['interval'].to_f * 60)).to_i

                    #launch garbage collector
                    GC.start
                end

                #return

                sleep(60) #sleep for 60 seconds (1 minute)
            end
        end

        # Protected methods
        protected

        def __config_file__()
            return @root + '/../etc/config.yaml'
        end

        def __schemas_folder__()
            return @root + '/../lib/schema'
        end
    end
end

if (__FILE__ == $0)
    Series::App.start
end
