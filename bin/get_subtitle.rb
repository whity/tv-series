#!/usr/bin/env rvm-shebang-ruby
#coding: utf-8

require('slop')
require('log4r')
require('mechanize')
require('xmlrpc/client')

SCRIPT_DIR = File.dirname(File.expand_path(__FILE__))

#lib paths
$LOAD_PATH.unshift(SCRIPT_DIR + '/../lib')

require('minds/pluggable')
require('minds/app')
require('minds/mixin/config')

require('subtitle')

#define logger
logger = Log4r::Logger.new('logger')
outputter = Log4r::Outputter.stdout
logger.outputters = outputter

class Script < Minds::App
    include(Minds::Mixin::Config)

    def initialize(*args)
        super(*args)

        #app root folder
        @root = SCRIPT_DIR
    end

    def run()
        #parse command line arguments
        opts = Slop.parse(ARGV) do |parser|
            parser.on(:serie,       'serie/tv-show/movie name', :argument => true)
            parser.on(:subtitle,    'subtitle file name',       :argument => true)
            parser.on(:'last-page', 'last page',                :argument => true, :default => -1, :as => :integer)
        end

        [:serie, :subtitle].each do |opt|
            if (!opts.present?(opt))
                raise(Exception, "missing argument '#{ opt }'")
            end
        end

        #get subtitle plugins
        plugins = ::Series::Subtitle.plugin

        #search for the pretended subtitle
        file = nil
        plugins.each do |plugin|
            #get plugin instance
            plugin = ::Series::Subtitle.plugin(plugin)

            #search
            file = plugin.new(self).search(
                :serie     => opts[:serie],
                :subtitle  => opts[:subtitle],
                :last_page => opts[:'last-page']
            )

            #shutdown plugin
            plugin.shutdown

            if (file)
                self.logger.info("subtitle found: #{ file }")
                return
            end
        end

        self.logger.info('no subtitle found')

        return
    end

    def logger()
        return Log4r::Logger['logger']
    end

    protected

    def __config_file__()
        return @root + '/../etc/config.yaml'
    end
end

Script.new.run
