#!/usr/bin/env rvm-shebang-ruby
#coding: utf-8

require( 'uri' )
require( 'log4r' )
require( 'mechanize' )
require( 'addressable/uri' )
require( 'net/http' )
require( 'uuid' )

SCRIPT_DIR = File.dirname( File.expand_path( __FILE__ ) )

#lib paths
$LOAD_PATH.unshift( SCRIPT_DIR + '/../lib' )

require( 'minds/schema' )
require( 'minds/transmission-client' )
require( 'schema/default' )

logger = Log4r::Logger.new('logger')

# Create an outputter for $stderr. It defaults to the root level WARN
outputter = Log4r::StderrOutputter.new( 'console' )

# format
pattern = Log4r::PatternFormatter.new( :pattern => "[%d %x] %l - %m" )
outputter.formatter = pattern

# add outputters
logger.add( 'console' )

# log level order is DEBUG < INFO < WARN < ERROR < FATAL
logger.level = Log4r::DEBUG

#download torrent file
def download_file(url)
    #download file
    response = nil
    begin
        uri  = URI.parse( url )
        http = Net::HTTP.new( uri.host, uri.port )
        http.open_timeout = 60
        http.read_timeout = 60

        response = http.start do |http|
            http.request_get( uri.path )
        end
    rescue Exception => ex
        #try with system wget
        process = IO.popen("wget -T 120 -q -O - \"$@\" \"#{ url }\"")
        response = process.readlines
        process.close

        if (response.length == 0)
            raise(Exception, "invalid torrent: #{ url }")
        end

        response = response.join("")
    end

    #generate file name
    filename = "/tmp/" + UUID.new.generate(:compact) + ".torrent"

    #open and write to temp file
    File.open(filename, "w") do |f|
        f.write(response)
    end

    return filename
end

def have_episode( episodes, season, episode )
    episodes.each do |ep|
        if (ep.season.name == season && ep.name == episode)
            return ep
        end
    end

    return nil
end

def have_season( seasons, season )
    seasons.each do |s|
        if (s.name == season)
            return s
        end
    end

    return nil
end

# serie eztv url
feed_url = 'https://eztv.ch/shows/1081/outlander/'

# init schema
schema = Series::Schema::Default.new(
    :dsn => 'sqlite://../app/series.db'
)

# indicate the wanted seasons
wanted_seasons = [] # - not specified means all

# get serie, season and episodes
serie    = schema.Serie.first( :name => '.Outlander' )
seasons  = serie.seasons
episodes = schema.Episode.where( :season => seasons ).all

# transmission client
transmission = Transmission::Client.new( '127.0.0.1', 9091 )

# start mechanize
ua = Mechanize.new
ua.get( feed_url ) do |page|
    rows = page.search( 'td.section_thread_post table.forum_header_noborder tr.forum_header_border' )
    rows.each do |row|
        begin
            title = row.css( '.epinfo' )[0]['title']

            logger.info( 'processing: ' + title )

            # fix season and episode format
            fix_format = title.match( /(\.|\s)*(\d+x\d+)(\.|\s)*/i )
            if ( fix_format )
                fix_format = fix_format[0]
                fix_format = fix_format.sub( /x/i, 'E' )
                fix_format = fix_format.sub( /(\d+E\d+)/i, 'S\1' )

                title = title.sub( /(\.|\s)*(\d+x\d+)(\.|\s)*/i, fix_format )
            end

            # get season and number
            matched = title.match( /^([\-\w\.\s]+)\s*S(\d+)E(\d+)/i )
            if ( !matched )
                logger.info( 'season and episode not found' )
                next
            end

            ( season_nbr, episode_nbr ) = matched[2 .. 3]

            # pad episode and season
            season_nbr  = season_nbr.rjust( 2, '0' )
            episode_nbr = episode_nbr.rjust( 2, '0' )

            # check if we already has this episode
            if ( have_episode( episodes, season_nbr, episode_nbr ) )
                logger.info( 'episode already available' )
                next
            end

            # check for season, if no active, continue to next item
            season = have_season( seasons, season_nbr )
            if ( ( wanted_seasons.length > 0 && !wanted_seasons.include?( season_nbr ) ) \
                || ( season && season.active == 0 ) )
                next
            elsif ( !season )
                season = schema.Season.new(
                    :name   => season_nbr,
                    :active => 1,
                )
            end

            # get torrent file
            torrent_file = nil
            row.css('a').each do |a|
                href = a['href']

                if ( !href.match( /(bt\-chat\.com|\.torrent)/i ) )
                    next
                end

                logger.info( 'torrent url: ' + href )
                uri = Addressable::URI.parse( href )

                if ( href.match(/bt\-chat\.com/i) )
                    # if param 'type' isn't defined, add it
                    query_values = uri.query_values
                    if ( !query_values.has_key?( 'type' ) )
                        query_values['type'] = 'torrent'
                    end
                    uri.query_values = query_values
                end

                # download torrent file
                torrent_file = download_file( uri.to_s )

                break
            end

            if ( torrent_file == nil )
                next
            end

            # add torrent file to transmission
            torrent = transmission.add_torrent_by_file( torrent_file )
            if ( !torrent )
                logger.error("invalid torrent, maybe already exist in transmission")
                next #some problem occured adding torrent, continue to next feed item
            end

            # let's save season and episode
            schema.db.transaction do
                # add season to serie if new
                if (season.new?)
                    serie.add_season( season )
                end

                # create the episode
                new_episode = schema.Episode.new(
                    :name         => episode_nbr,
                    :description  => title,
                    :status       => 'D',
                    :torrent_hash => torrent['hashString'],
                    :torrent_file => torrent_file
                )
                season.add_episode( new_episode )

                #if season doesn't exists on seasons add it
                if ( !seasons.include?(season) )
                    seasons.push( season )
                end

                episodes.push( new_episode )
            end

            logger.info("new episode downloading: #{ title }")
        rescue Exception => ex
            logger.error( ex )
        end
    end
end
