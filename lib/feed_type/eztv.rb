module Series
    module FeedType
        class EZTV < Plugin
            #Protected methods
            protected

            def self._can_handle()
                return [Regexp.new('ezrss', Regexp::IGNORECASE)]
            end
        end
    end
end
