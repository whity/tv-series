module Series
    module FeedType
        class Btchat < Plugin
            #Protected Methods
            protected

            def self._can_handle()
                return [Regexp.new('bt-chat', Regexp::IGNORECASE)]
            end
        end
    end
end
