module Series
    module FeedType
        extend(Minds::Pluggable)

        class Plugin
            attr_reader(:serie, :season, :episode, :torrents)

            class << self
                def handle?(feed_url)
                    handles_regexp = self._can_handle
                    handles_regexp.each do |handle_re|
                        if (feed_url.match(handle_re))
                            return true
                        end
                    end

                    return false
                end

                # Protected Methods
                protected

                def _can_handle()
                    #return an array of regexp to test against feed_url
                    return Array.new
                end

            end

            #constructor
            def initialize(item)
                @serie    = nil
                @season   = nil
                @episode  = nil
                @torrents = nil
                @_xvid    = true

                title = item.title

                # fix season and episode format
                fix_format = title.match( /(\.|\s)*(\d+x\d+)(\.|\s)*/i )
                if ( fix_format )
                    fix_format = fix_format[0]
                    fix_format = fix_format.sub( /x/i, 'E' )
                    fix_format = fix_format.sub( /(\d+E\d+)/i, 'S\1' )

                    title = title.sub( /(\.|\s)*(\d+x\d+)(\.|\s)*/i, fix_format )
                end

                #get season and number
                #re = self._serie_season_episode_regexp
                matched = title.match( /^([\-\w\.\s]+)\s*S(\d+)E(\d+)/i )
                if (matched)
                    (@serie, @season, @episode) = matched[1 .. 3]

                    #remove last char from serie if it's a .(dot)
                    @serie = @serie.strip.gsub(/\.$/, '')

                    #pad episode and season
                    @season = @season.rjust(2, '0')
                    @episode = @episode.rjust(2, '0')
                end

                #check if is a 720p it's not a xvid
                matched = item.title.match(/720p/i)
                if (matched)
                    @_xvid = false
                end

                #@torrents = item.enclosures
            end

            def xvid?()
                return @_xvid
            end

            # Protected Methods
            protected

            def _serie_season_episode_regexp()
                return Regexp.new("", Regexp::IGNORECASE)
            end
        end

        #init plugins
        _pluggable_setup(
            :require => true
        )

        class << self
            def plugin(name=nil)
                return self._pluggable_get(name)
            end

            def handler(feed_url)
                self.plugin.each do |plugin|
                    plugin = self.plugin(plugin)

                    if (plugin.handle?(feed_url))
                        return plugin
                    end
                end

                return nil
            end
        end
    end
end
