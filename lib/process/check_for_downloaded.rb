module Series
    module Process
        #check for new episodes to download
        class CheckForDownloaded < Plugin
            #Protected Methods
            protected

            def _run()
                #connect to transmission
                transmission = self._transmission

                #get transmission torrents
                torrents = transmission.torrents

                #get episodes with status 'D (downloading)'
                episodes = @_app.schema.Episode.where(:status => 'D').all

                #process each episode
                episodes.each do |ep|
                    #get torrent by id
                    torrent = nil
                    torrent_index = nil
                    torrents.each_with_index do |tor, idx|
                        if (tor.hashString == ep.torrent_hash)
                            torrent = tor
                            torrent_index = idx
                            break #stop loop, torrent found
                        end
                    end
                    ##

                    #if torrent object not found, continue to the next episode
                    # or
                    # torrent didn't complete the download, continue to the next episode
                    if (!torrent || torrent.percentDone.to_i < 1)
                        if (!torrent) #torrent not found, but already downloaded
                            ep.status = 'S'
                            ep.save
                        end

                        next #continue to the next episode
                    end

                    #stop torrent
                    torrent.stop

                    #move files to folder
                    self._move_episode_files(torrent, ep)

                    #remove torrent from transmission with delete data and from array torrents
                    torrent.remove(true)
                    torrents.delete_at(torrent_index)

                    #delete torrent file
                    if (File.exists?(ep.torrent_file))
                        File.unlink(ep.torrent_file)
                    end

                    #update episode status to S (subtitles) and set torrent_id no NUll
                    ep.status = 'S'
                    ep.torrent_hash = nil
                    ep.torrent_file = nil

                    #save episode
                    ep.save

                end #loop end

            end

            #copy episode files to serie season folder
            def _move_episode_files(torrent, episode)
                tor_files = torrent.files

                #build episode folder
                ep_folder = self._build_episode_folder(episode, tor_files.length)

                #copy files to episode folder
                tor_files.each do |tor_file|
                    # build source file name
                    source_file = torrent.downloadDir + "/#{ tor_file.name }"

                    ## build destination file name
                    # get only file name
                    destination_filename = tor_file.name.match( /([^\/]*)$/i )[1]

                    # destination file name path
                    destination_file = ep_folder + "/" + destination_filename

                    # copy to serie-season folder
                    FileUtils.cp(source_file, destination_file)
                end
            end

            #check if destination episode folder exists, if not create it
            def _build_episode_folder(episode, nbr_files)
                ep_path = episode.season.serie.name + "/" + episode.season.name
                #if (nbr_files > 1)
                #    ep_path += "/" + episode.name
                #end

                full_path = "#{ @_app.config.get('media_folder') }/#{ ep_path }"

                #certify that the full path exists, if not, creates it
                FileUtils.makedirs(full_path)

                return full_path
            end

        end #end class
    end
end
