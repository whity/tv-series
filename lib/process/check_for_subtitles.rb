module Series
    module Process
        #check for new episodes subtitles
        class CheckForSubtitles < Plugin
            # protected Methods
            protected

            def _get_extracted(file, subtitle)
                #compressions supported
                cmd_extension = {
                    ".zip" => "unzip %s -d %s",
                    ".rar" => "unrar x %s %s"
                }

                #get file extension
                ext = File.extname(file)

                #get right command to execute
                cmd = cmd_extension[ext]

                tmpdir = nil
                begin
                    #create temporary directory
                    tmpdir = Dir.mktmpdir

                    #build right command to execute
                    cmd = sprintf(cmd, file, tmpdir)

                    # extract file to a tmpdir
                    output = nil
                    IO.popen(cmd, :err => [:child, :out]) do |process|
                        #read output
                        output = process.read
                    end
                    if ($? && $?.exitstatus > 0)
                        raise(Series::Exceptions::CmdException, output)
                    end

                    #get the srt files
                    files = Dir.glob(tmpdir + '/**/*.srt')

                    #if only one file is found, use that one, search for the most similar
                    if (files.length == 1)
                        tmpfile = files[0]
                    else
                        files.each do |fl|
                            #it's a file test it
                            if (fl.match(/#{ Regexp.escape(subtitle) }/i))
                                tmpfile = fl
                                break
                            end
                        end
                    end

                    if (tmpfile)
                        #move file to tmp root
                        FileUtils.move(tmpfile, '/tmp/')

                        #correct the file path, ????
                        begin
                            file = '/tmp/' + tmpfile.split('/')[-1]
                        rescue ArgumentError => ex
                            tmpfile.force_encoding('iso-8859-1')
                            file = '/tmp/' + tmpfile.split('/')[-1]
                        end
                    end
                ensure
                    if (tmpdir)
                        #delete temporary directory
                        FileUtils.remove_entry_secure(tmpdir)
                    end
                end

                return file
            end

            def _run()
                check_on_plugins = @_app.config['subtitles'].keys

                #media folder
                media_folder = @_app.config['media_folder']

                #get all episodes with status 'S'
                episodes = @_app.schema.Episode.eager_graph(:season => :serie).where(
                    :status        => 'S',
                    :serie__active => 1
                ).all

                check_on_plugins.each do |plugin_name|
                    #if no episodes available, stop plugins loop
                    if (episodes.size == 0)
                        break
                    end

                    plugin = nil
                    begin
                        plugin = Series::Subtitle.plugin(plugin_name).new(@_app)

                        #plugin partial name
                        plugin_name = plugin.class.name.split('::')
                        plugin_name = plugin_name[-1]

                        Log4r::NDC.push(plugin_name)

                        #check for subtitle for each episode
                        idx = episodes.size - 1
                        while (idx >= 0)
                            episode = episodes[idx]

                            begin
                                #get episode file
                                self._episode_file(episode)

                                file = plugin.search(:episode => episode)
                                if (file == nil)
                                    next
                                end

                                #remove extension from episode
                                subtitle = episode.description.gsub(/\.\w+$/i, '')

                                #remove release signature
                                subtitle = subtitle.gsub(/\.?\[\w*\]/i, '')

                                @_app.logger.info("subtitle found: #{ subtitle }")

                                #subtitle found and downloaded, let's move serie and season folder
                                extracted_file = self._get_extracted(file, subtitle)

                                #move file to destination folder
                                destination_file = media_folder + '/' + episode.folder
                                if (extracted_file != file)
                                    destination_file = episode.file.sub(/\.\w+$/i, File.extname(extracted_file))
                                end

                                #move file to destination
                                FileUtils.move(extracted_file, destination_file)

                                #set episode has complete
                                episode.status = 'C'
                                episode.save

                                #remove episode from queue
                                episodes.delete_at(idx)
                            rescue ::Series::Exceptions::EpisodeFileNotFoundException => ex
                                #episode file don't exist
                                @_app.logger.warn("episode file missing: '#{ episode.description }' ")

                                #remove episode from queue
                                episodes.delete_at(idx)
                            rescue ::Series::Exceptions::SubtitlesSearchException => ex
                                @_app.logger.warn(ex.message)
                                @_app.logger.debug(ex.details)
                            ensure
                                idx -= 1
                            end
                        end

                        Log4r::NDC.pop
                    ensure
                        if (plugin != nil)
                            #shutdown plugin, clean plugin resources
                            plugin.shutdown
                        end
                    end
                end

                return nil
            end

            def _episode_file(episode)
                #build episode folder
                episode_folder = @_app.config['media_folder'] + '/' + episode.folder

                #search for episode file
                files = Dir.glob(episode_folder + "/*E#{ episode.name }*", File::FNM_CASEFOLD)
                if (files.length <= 0)
                    raise(::Series::Exceptions::EpisodeFileNotFoundException)
                end

                # get the movie file
                file = nil
                files.each do |fl|
                    mime_type = FileMagic.new( FileMagic::MAGIC_MIME ).file( fl )
                    if ( mime_type.match( /video/i ) )
                        file = fl
                        break
                    end
                end

                #set attribute on episode object
                #episode.instance_variable_set(:@file, files[0])

                #set attribute on episode object
                episode.define_singleton_method(:file) do
                    file
                end

                return
            end
        end #end class
    end
end
