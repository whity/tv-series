module Series
    module Process
        class CheckForNew < Plugin
            Feedjira::Feed.add_common_feed_entry_element('torrent:magnetURI', :as => :magnet)

            # Protected Methods
            protected

            def _run()
                #connect to transmission
                transmission = self._transmission

                ###PROCESS###
                feeds = @_app.config.get('feed_sources')
                feeds.each_with_index do |value, idx|
                    feeds[idx] = {
                        :source => value,
                        #:data   => FeedNormalizer::FeedNormalizer.parse(open(value)),
                        :data => Feedjira::Feed.fetch_and_parse( value ),
                        :type   => nil
                    }
                end

                #get active series
                active_series = @_app.schema.Serie.where(:active => 1).all
                active_series.each do |serie|
                    serie_feeds = Array.new

                    #check if serie has it's own feed url
                    if (serie.feed_url != nil && serie.feed_url.length > 0)
                        begin
                            serie_feeds.push({
                                :source => serie.feed_url,
                                :data   => FeedNormalizer::FeedNormalizer.parse(open(serie.feed_url)),
                                :type   => nil
                            })
                        rescue Exception => ex
                            @_app.logger.error("error reading serie source: #{ serie.feed_url }, using defaults")
                            serie_feeds = feeds
                        end
                    else
                        serie_feeds = feeds
                    end

                    #get all seasons of the serie
                    seasons = serie.seasons

                    #get all episodes of the serie
                    episodes = @_app.schema.Episode.where(:season => seasons).all

                    #process feeds
                    serie_feeds.each do |feed|
                        if (feed[:type] == false)
                            next #continue to the next feed source
                        end

                        item_type = feed[:type]
                        if (item_type == nil)
                            begin
                                #check feed items type
                                item_type = self._feed_type(feed[:source])
                                feed[:type] = item_type
                            rescue Exception => ex
                                #error checking type of feed item to process, continue to next feed
                                @_app.logger.error("error checking feed type: " + ex.message)
                                feed[:type] = false
                                next
                            end
                        end

                        #check each item of feed
                        feed[:data].entries.each do |item|
                            begin
                                item_info = item_type.new(item)

                                #if isn't xvid release, or doesnt belong to the serie discard it
                                if (!item_info.xvid? || !serie.eq_name?(item_info.serie))
                                    next
                                end

                                #check if we already has this episode
                                if (self._have_episode(episodes, item_info.season, item_info.episode))
                                    next
                                end

                                #check for season, if no active, continue to next feed item
                                season = self._have_season(seasons, item_info.season)
                                if (season && season.active == 0)
                                    next
                                elsif (!season)
                                    season = @_app.schema.Season.new(
                                        :name   => item_info.season,
                                        :active => 1,
                                    )
                                end

                                #add torrent file to transmission
                                torrent = transmission.add_torrent_by_file( item.magnet )
                                if (!torrent)
                                    @_app.logger.error("invalid torrent, maybe already exist in transmission")
                                    next #some problem occured adding torrent, continue to next feed item
                                end

                                #let's save season and episode
                                @_app.schema.db.transaction do
                                    #add season to serie if new
                                    if (season.new?)
                                        serie.add_season(season)
                                    end

                                    #create the episode
                                    new_episode = @_app.schema.Episode.new(
                                        :name         => item_info.episode,
                                        :description  => item.title,
                                        :status       => 'D',
                                        :torrent_hash => torrent['hashString'],
                                        :torrent_file => tor_file
                                    )
                                    season.add_episode(new_episode)

                                    #if season doesn't exists on seasons add it
                                    if (!seasons.include?(season))
                                        seasons.push(season)
                                    end

                                    episodes.push(new_episode)
                                end

                                @_app.logger.info("new episode downloading: #{ item.title }")
                            rescue Exception => ex
                                raise ex
                                @_app.logger.error("error processing '#{ item.title }': #{ ex.message() }")
                            end

                        end #end feed items
                    end #end feeds
                end #end series loop
            end

            def _have_episode(episodes, season, episode)
                episodes.each do |ep|
                    if (ep.season.name == season && ep.name == episode)
                        return ep
                    end
                end

                return nil
            end

            def _have_season(seasons, season)
                seasons.each do |s|
                    if (s.name == season)
                        return s
                    end
                end

                return nil
            end

            #check the type/domain of the feed
            def _feed_type(feed_url)
                #select the type that processes the current feed url
                feed_type = ::Series::FeedType.handler(feed_url)
                if (feed_type == nil)
                    raise(Exception, "no type supported for feed: #{ feed_url }")
                end

                return feed_type
            end
        end
    end
end
