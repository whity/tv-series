require('yaml')
require_relative('./hash')

module Minds
    module Config
        class YAML < Hash
            def initialize(file: nil)
                #check if config file exists and read it
                if (file == nil || !File.exist?(file))
                    raise(ArgumentError, "invalid file '#{ file }'")
                end

                tmp_data = ::YAML.load_file(file)
                if (tmp_data == nil)
                    tmp_data = ::Hash.new
                end

                super(:data => tmp_data)
            end
        end
    end
end
