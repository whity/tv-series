module Minds
    module Pluggable
        module Exception
            class InvalidPluginException < ::Exception
            end
        end
        
        # Protected methods        
        protected

        def _pluggable_build_path()
            #class name
            class_name = self.name.match(/([^:]+)$/i)[1]

            #build folder name, on upper case chars prepend '_'
            filename = ::Array.new
            idx = 0
            while (idx < class_name.length)
                char = class_name[idx]
                if (idx > 0 && char.ord >= 65 && char.ord <= 90)
                    filename.push('_')
                end

                filename.push(char.downcase)
                idx += 1
            end
            
            return File.dirname(caller_locations[1].absolute_path) + '/' + filename.join('')
        end

        def _pluggable_setup(**config)
            if (self.class_variable_defined?(:@@_pluggable))
                tmp_config = self.class_variable_get(:@@_pluggable)
                tmp_config.delete(:path)
                config = tmp_config.merge(config)
            end

            if (!config[:path])
                config[:path] = self._pluggable_build_path
            end

            self.class_variable_set(:@@_pluggable, config)

            #load plugins
            if (!config[:plugins])
                config[:plugins] = ::Hash.new
            end
            
            config[:plugins].merge!(self._pluggable_load_dir(config[:path], config[:require]))
            
            #plugin names
            config[:plugins_names] = Hash.new
            config[:plugins].each do |name, plugin|
                 plugin[:names].each do |alternative|
                     config[:plugins_names][alternative] = name
                 end
            end 

            return
        end

        def _pluggable_get(plugin=nil)
            self._pluggable_check_setup

            #get config
            config = self.class_variable_get(:@@_pluggable)

            #check if plugin was provided
            if (plugin != nil)
                plugin = plugin.to_sym

                #check if plugin is already loaded, if not, load it
                if (!self._pluggable_plugin_loaded?(plugin))
                    plugin = config[:plugins_names][plugin]
                    self._pluggable_load_plugin(config[:plugins][plugin])
                else #plugin already loaded
                    plugin = config[:plugins_names][plugin]
                end

                return config[:plugins][plugin][:object]
            end

            return config[:plugins].keys
        end

        def _pluggable_check_setup()
            #ensure we've done setup
            if (!self.class_variable_defined?(:@@_pluggable))
                raise(::Exception, "pluggable not configured")
            end

            return
        end

        def _pluggable_plugin_exist?(plugin)
            self._pluggable_check_setup

            if (plugin == nil)
                raise(ArgumentError, "plugin name not provided")
            end

            #get config
            config = self.class_variable_get(:@@_pluggable)

            name = config[:plugins_names][plugin.to_sym]
            if (name)
                return true
            end

            return false
        end

        def _pluggable_plugin_loaded?(plugin)
            self._pluggable_check_setup

            #get plugin key/name
            if (!self._pluggable_plugin_exist?(plugin))
                raise(Exception::InvalidPluginException, "invalid plugin '#{ plugin }'")
            end

            #get config
            config = self.class_variable_get(:@@_pluggable)

            name = config[:plugins_names][plugin.to_sym]
            if (config[:plugins][name][:object])
                return true
            end

            return false
        end

        def _pluggable_load_dir(dir, load_plugin=false, parent=[])
            plugins = Hash.new

            dir = Dir.new(dir)
            dir.each do |item|
                #discard items that start by dot(.)
                if (item[0] == '.')
                    next
                end

                full_item_path = dir.path + '/' + item
                if (File.directory?(full_item_path))
                    plugins.merge!(self._pluggable_load_dir(full_item_path, load_plugin, parent + [item]))
                elsif (File.extname(item) == '.rb')
                    #generate all possible names for the plugin
                    #save the object, to avoid searching when requesting it, make it on _load_plugin
                    plugin_names = self._pluggable_plugin_names(parent + [item])
                    plugin = {
                        :names     => plugin_names,
                        :filename  => full_item_path,
                        :object    => nil,
                        :namespace => self
                    }
                    
                    register = true
                    if (load_plugin)
                        begin
                            self._pluggable_load_plugin(plugin)
                        rescue Exception::InvalidPluginException => ex
                            #don't register plugin
                            register = false
                        end
                    end
    
                    if (register)
                        #use first name as main
                        name = plugin_names[0]
                        plugins[name] = plugin
                    end
                end
            end

            return plugins
        end

        def _pluggable_plugin_names(item)
            file = item.shift

            #remove extension
            file.sub!(/\.\w+$/i, '')

            ###name possibilities
            names = [file.upcase.to_sym]

            #capitalize
            file_split = file.split(/_/)
            file_split.map! { |it| it.capitalize }
            names.unshift(file_split.join('').to_sym)
            ###

            result = names
            if (item.length > 0)
                result = ::Array.new
                restant_names = self._pluggable_plugin_names(item.clone)
                names.each do |name|
                    restant_names.each do |sub_name|
                        result.push("#{ name }::#{ sub_name }".to_sym)
                    end
                end
            end

            return result
        end

        def _pluggable_load_plugin(plugin)
            #if plugin already loaded, don't do nothing
            if (plugin[:object] != nil)
                return
            end

            #require file
            require(plugin[:filename])

            #search for the object
            names = plugin[:names]
            names.each do |name|
                namespace = plugin[:namespace]
                tmp_object = namespace
                name.to_s.split("::").each do |item|
                    if (tmp_object.const_defined?(item))
                        tmp_object = tmp_object.const_get(item)
                    else
                        tmp_object = nil
                        break
                    end
                end

                #if tmp_object isn't nil, indicates that we found the object, stop names loop
                if (tmp_object)
                    plugin[:object] = tmp_object
                    break
                end
            end

            if (plugin[:object] == nil)
                raise(Exception::InvalidPluginException, "invalid plugin: #{ names.join(',') }")
            end

            return
        end

        def _pluggable_register_plugin(cls, **kwargs)
            # => get the class name
            # => get the namespace
            # => build the alternative names

            namespace = cls.to_s.sub(/^::/i, '') #remove starting separator, if exists
            namespace = namespace.split('::') #split by '::'
            name = namespace.pop #the name is the last position of namespace
            namespace = Kernel.const_get(namespace.join("::"))

            names = kwargs[:names]
            if (!names)
                names = self._pluggable_plugin_names([name])
            end

            #ensure all names are symbols
            names = names.map {|item| item.to_sym }

            #first name is the pointer
            name = names[0]

            plugin = {
                :names     => names,
                :namespace => namespace,
                :object    => cls,
                :filename  => nil
            }

            #get config
            config = self.class_variable_get(:@@_pluggable)

            #register plugin and names
            config[:plugins][name] = plugin
            names.each do |item|
                config[:plugins_names][item] = name
            end

            return plugin
        end
    end
end
