module Series
    module Exceptions
        class CmdException < Exception
        end

        class EpisodeFileNotFoundException < Exception
        end

        # Subtitles
        class SubtitlesSearchException < Exception
            attr_reader(:details)

            def initialize(msg, details)
                super(msg)

                @details = details
            end
        end
    end
end
