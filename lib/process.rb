module Series
    module Process
        extend(Minds::Pluggable)

        class Plugin
            def initialize(app)
                @_app = app
            end

            class << self
                def inherited(subclass)
                    subclass.class_variable_set(
                        :@@_name,
                        subclass.name.match(/::([^:]+)$/)[1]
                    )
                end

                def process_name()
                    return self.class_variable_get(:@@_name)
                end
            end


            def run()
                #get logger
                Log4r::NDC.push(self.class.process_name)
                @_app.logger.info("start")

                begin
                    self._run
                    @_app.logger.info("done")
                rescue Exception => ex
                    @_app.logger.error(ex.message)
                    @_app.logger.debug(ex.message + "\n" + ex.backtrace.join("\n"))
                end

                Log4r::NDC.pop
            end

            #Protected Methods
            protected

            def _run()
                raise(::NotImplementedError)
            end

            def _transmission()
                return Transmission::Client.new('127.0.0.1', 9091)
            end
        end

        #init plugins
        _pluggable_setup(
            :require => true
        )

        class << self
            def plugin(name=nil)
                return self._pluggable_get(name)
            end
        end
    end
end
