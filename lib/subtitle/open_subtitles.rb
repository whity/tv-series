module Series
    module Subtitle
        class OpenSubtitles < Plugin
            #constructor
            def initialize(*args)
                super(*args)

                @_rpc = XMLRPC::Client.new(@_config['rpc']['host'], @_config['rpc']['path'])

                #login
                response = @_rpc.call('LogIn', '', '', 'en', @_config['rpc']['user_agent'])

                @_token = response['token']
            end

            #search for subtitle
            def search(**kwargs)
                api_method = 'SearchSubtitles'
                api_search = {
                    :sublanguageid => @_config['sublanguageid']
                }

                if (kwargs[:episode])
                    episode = kwargs[:episode]

                    api_search.merge!({
                        :moviehash     => ::Series::MovieHasher.compute_hash(episode.file),
                        :moviebytesize => ::File.size(episode.file).to_s,
                    })
                else
                    #use serie name and subtitle file name
                    api_search.merge!({
                        :query => kwargs[:subtitle]
                    })
                end

                #search on api
                response = self._api(api_method, api_search)

                #check for error
                if (!response['status'].match(/200 OK/i))
                    raise(::Series::Exceptions::SubtitlesSearchException.new(
                        response['status'],
                        {:method => method}.merge(api_search)
                    ))
                end

                #if data is empty, subtitle not found
                if (!response['data'] || response['data'].length == 0)
                    return
                end
                
                @_app.logger.debug( response.inspect )

                #download file
                response = Net::HTTP.get_response(URI(response['data'][0]['ZipDownloadLink']))
                if (!response.kind_of?(Net::HTTPSuccess))
                    raise(::Series::Exceptions::SubtitlesSearchException.new(
                        'error downloading subtitle',
                        response
                    ))
                end

                #save on tmp
                tempfile = '/tmp/' + UUID.new.generate + '.zip'
                File.open(tempfile, 'w') do |fl|
                    fl.puts(response.body)
                end

                return tempfile
            end

            def shutdown()
                return self._api('LogOut')
            end

            # Protected Methods
            protected

            def _api(method, *args)
                return @_rpc.call(method, @_token, args)
            end
        end
    end
end
