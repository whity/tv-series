module Series
    module Subtitle
        class LegendasXvid < Plugin
            #constructor
            def initialize(*args)
                super(*args)

                #create agent object
                @_agent = ::Mechanize.new
                @_agent.history.max_size = 0 #no page history

                #get page legendasdivx
                @_page = @_agent.get(URI('http://www.legendasdivx.com'))

                #go to login page
                @_page = @_page.link_with(:text => /a\s+sua\s+conta/i).click

                #get login form
                login_form = @_page.form_with(:action => /modules\.php\?name=Your_Account/i)

                #login in the page
                login_form['username'] = @_config['username']
                login_form['user_password'] = @_config['password']
                @_page = login_form.submit

                #search form
                @_search_form = @_page.form_with(:name => 'formiframe')
            end

            def search(**kwargs)
                if (kwargs[:episode])
                    serie = kwargs[:episode].season.serie.name
                    subtitle = kwargs[:episode].description.sub(/\.[^\.]*$/i, '')
                else
                    #strip start and ending spaces
                    serie = kwargs[:serie].strip
                    subtitle = kwargs[:subtitle].strip
                end

                #remove version from subtitle
                subtitle = subtitle.sub(/\.\[\w*\]/i, '')

                #get the last page to check, -1 means no limit
                last_page = kwargs[:last_page] || @_config['last_page'] || 5

                @_search_form['query'] = serie
                @_search_form.radiobutton_with(:name => 'form_cat', :value => 28)
                result_page = @_search_form.submit

                #search for the right subtitle to download
                tmp_file = nil
                catch(:subtitle) do
                    legends = result_page.search('.sub_box')
                    page_nbr = 1
                    while (legends.size > 0 && (last_page == -1 || page_nbr <= last_page))
                        @_app.logger.debug("page: #{ page_nbr }, subtitle: #{ subtitle }")
                        legends.each do |item|
                            description = item.at('.td_desc').text

                            #search in description for the pretended subtitle
                            if (description.match(/#{ Regexp.escape(subtitle) }/i))
                                #if the subtitle is found, download the file
                                link = item.at('.sub_download').attributes['href'].value
                                result = result_page.link_with(:href => /#{ Regexp.escape(link) }/i).click

                                #filename extensions
                                #save file contents to episode folder
                                tmp_file = '/tmp/' + subtitle + File.extname(result.filename)
                                result.save(tmp_file)

                                throw(:subtitle) #stop loop, subtitle found and processed
                            end
                        end

                        #next page
                        next_link = Mechanize::Page::Link.new(
                            result_page.search('.pager_bar').children[-1],
                            @_agent,
                            result_page
                        )
                        result_page = next_link.click()
                        legends = result_page.search('.sub_box')
                        page_nbr += 1
                    end
                end

                return tmp_file
            end

            def shutdown()
                @_agent.shutdown
                @_agent = nil
                @_search_form = nil
                @_page = nil
                @_config = nil
            end
        end
    end
end
