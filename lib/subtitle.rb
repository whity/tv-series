module Series
    module Subtitle
        extend(Minds::Pluggable)

        class Plugin
            def initialize(app)
                @_app = app

                #config
                config_section = self.class.to_s
                config_section = config_section.match(/::(\w*)$/i)[1]
                @_config = @_app.config.get('subtitles')
                if (@_config.kind_of?(Hash) && @_config.has_key?(config_section))
                    @_config = @_config[config_section]
                else
                    @_config = Hash.new
                end
                @_config.freeze
            end

            def search()
                raise(::NotImplementedError)
            end

            def shutdown()
                #by default don't do nothing
            end
        end

        #init plugins
        _pluggable_setup(
            :require => true
        )

        class << self
            def plugin(name=nil)
                return self._pluggable_get(name)
            end
        end
    end
end
