module Series
    module Schema
        class Default < Minds::Schema
            def initialize(kwargs={})
                #set dsn
                if (kwargs[:dsn].match(/^sqlite:\/\//i))
                    path = kwargs[:dsn].match(/^sqlite:\/\/(.+)$/i)[1]
                    path = File.expand_path(path)

                    kwargs[:dsn] = 'sqlite://' + path
                end

                super(kwargs)
            end

            def self.search_dirs()
                return [File.expand_path(__FILE__).gsub(/\.\w*$/i, '')]
            end
        end
    end
end
