class Serie < Sequel::Model(:tbSeries)
    def self.__setup__(namespace)
        #create associations
        self.one_to_many(:seasons, :class => namespace::Season, :key => :serie_id)
    end

    def eq_name?(name)
        2.times() do |idx|
            tmp_name = (idx == 0) ? self.name : self.alias
            if (tmp_name == nil || tmp_name.length == 0)
                next #continue to the next iteration
            end

            tmp_name_spaces = tmp_name.gsub(/\./, ' ')
            if (tmp_name == name || tmp_name_spaces == name)
                return true
            end
        end

        return false
    end
end
