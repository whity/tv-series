class Season < Sequel::Model(:tbSeasons)
    def self.__setup__(namespace)
        #create associations
        self.many_to_one(:serie, :class => namespace::Serie, :key => :serie_id)
        self.one_to_many(:episodes, :class => namespace::Episode, :key => :season_id)
    end
end
