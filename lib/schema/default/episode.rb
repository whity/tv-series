class Episode < Sequel::Model(:tbEpisodes)
    def self.__setup__(namespace)
        #create associations
        self.many_to_one(:season, :class => namespace::Season, :key => :season_id)
    end

    def folder()
        return self.season.serie.name + "/" + self.season.name
    end
end
